<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'ILSManager') }}</title>

    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=jquery.js" defer></script>
    <script type="text/javascript" src="https://www.linux.it/shared/index.php?f=bootstrap.js" defer></script>
    <script src="{{ asset('js/jquery.inputmask.min.js') }}" defer></script>

    <link href="https://www.linux.it/shared/?f=bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="https://www.linux.it/shared/?f=main.css" rel="stylesheet" type="text/css" />
    <link href="{{ asset('css/open-iconic-bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/ilsmanager.css') }}" rel="stylesheet">
    @if(Request::is('user'))
    <link href="{{ asset('css/jquery.dataTables.min.css') }}" rel="stylesheet">
    <script src="{{ asset('js/jquery.dataTables.min.js') }}" defer></script>
    <script src="{{ asset('js/chart.js') }}" defer></script>
    @endif
    <script src="{{ asset('js/ilsmanager.js') }}" defer></script>
</head>
<body>
    <div id="app">
        <div id="header">
            <a href="{{ url('/') }}">
                <img src="{{ asset('images/logo.png') }}" alt="Logo ILSManager" />
                <div id="maintitle"> {{ config('app.name', 'ILSManager') }}</div>
                <div id="payoff">Gestione dell'associazione</div>
            </a>

            <div class="menu">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="main-menu">
                        <ul class="navbar-nav mr-auto">
                            @guest
                                <li><a class="nav-link" href="{{ route('login') }}">Login</a></li>
                                <li><a class="nav-link" href="{{ route('register') }}">Iscriviti</a></li>
                            @else
                                <li><a class="nav-link" href="{{ url('/') }}">Home</a></li>
                                <li class="nav-item dropdown">
                                    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                        {{ $currentuser->username }} <span class="caret"></span>
                                    </a>

                                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                        <a class="dropdown-item" href="{{ route('user.edit', $currentuser->id) }}">Dati Personali</a>

                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </div>
                                </li>
                            @endguest
                        </ul>
                    </div>
                </nav>

                <p class="social mt-2">
                    <a href="https://twitter.com/ItaLinuxSociety"><img src="https://www.linux.it/shared/?f=immagini/twitter.svg" alt="ILS su Twitter"></a>
                    <a href="https://www.facebook.com/ItaLinuxSociety/"><img src="https://www.linux.it/shared/?f=immagini/facebook.svg" alt="ILS su Facebook"></a>
                    <a href="https://www.instagram.com/ItaLinuxSociety"><img src="https://www.linux.it/shared/?f=immagini/instagram.svg" alt="ILS su Instagram"></a>
                    <a href="https://mastodon.uno/@ItaLinuxSociety/"><img src="https://www.linux.it/shared/?f=immagini/mastodon.svg" alt="ILS su Mastodon"></a>
                    <a href="https://gitlab.com/ItalianLinuxSociety"><img src="https://www.linux.it/shared/?f=immagini/gitlab.svg" alt="ILS su GitLab"></a>
                </p>
            </div>
        </div>

        @if ($errors->any())
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
