$(function() {

    // This ID for the internal account "Quote Associative".
    // TODO: Do not hardcode this :D
    var CONFIG_ACCOUNT_FOR_FEES_ID = 2;

    $('.show-all').click(function() {
        var target = $(this).attr('data-target');
        $(target).find('.hidden').toggle();
    });

    $('.add-row').click(function(e) {
        e.preventDefault();
        var row = $(this).siblings('.hidden').find('li').clone();
        $(this).closest('.list-group-item').before(row);
    });

    /**
     * Generic action to be placed inside a container, to drop it.
     * This works also on dynamically created elements.
     * The "Delete" button should match this selector:
     *    .list-group-item .delete-row
     * Its container should match this selector instead:
     *    .list-group-item
     */
    $('body').on('click', '.delete-row', function(e) {
        e.preventDefault();
        $(this).closest('.list-group-item').remove();
    });

    $('.multiple-files').on('change', 'input:file', function() {
        $(this).after('<input type="file" name="file[]" autocomplete="off">');
    });

    $("input.date").inputmask({
        'mask': '9999-99-99',
        'placeholder': 'yyyy-mm-dd',
        'clearIncomplete': true,
    });

    (function() {
        /**
         * Allow to refresh User info when one is selected.
         *
         * When an User is selected, refresh the info box below.
         * At the moment probably this is just used by this file:
         * Note that AccountRows can be created on-the-fly, so we're using "on('change')".
         * @page /ng/movement/review
         * @page /ng/movement/1/edit
         * @view resources/views/accountrow/editblock.blade.php
         * https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/18
         */
        function updateMovementUserHints(context) {
            var $context = $(context);
            var $container = $context.closest('.ils-container-user-select');
            var $accountRowContainer = $context.closest('.account-row-container');
            var $selected  = $context.find(':selected');
            var $userInfo  = $container.find('.ils-container-user-info');
            var $accountRowContainer = $context.closest('.list-group-item');
            var $bankAccount = $accountRowContainer.find('select[name="account[]"]');
            var bankAccountSelected = parseInt($bankAccount.find(':selected').val());
            var userId     = $selected.val();
            var userName   = $selected.text();
            var userBase   = $userInfo.data('userbaseurl');
            if( userId && userId != 0 ) {

                // in the template there is a fake Id just to be used as template
                var userURL = userBase.replace(666, userId);

                $userInfo.find('.ils-user-link')
                         .prop('href', userURL);

                $userInfo.find('.ils-user-displayname')
                         .text(userName);

                $userInfo.show('fast');

                // Automagically pick the "Quote Associative" bank account :D
                if (bankAccountSelected === 0) {
                    $bankAccount.val(CONFIG_ACCOUNT_FOR_FEES_ID)
                                .change();
                }
            } else {
                $userInfo.hide('fast');
            }
        }

        // Do something on click, also on elements that are not already in the page.
        $('body').on('change', '.ils-container-user-select .ils-user-select', function() {
              updateMovementUserHints(this);
        });

        // Do something also at startup on already-existing DOM elements.
        $('.ils-container-user-select .ils-user-select').each(function(i, select) {
               // This class may be present at startup.
              var $select = $(select);
              var $container = $select.closest('.ils-container-user-select');
              var $userInfo  = $container.find('.ils-container-user-info');
              $userInfo.hide();
              $userInfo.removeClass('d-none');
              updateMovementUserHints($select);
        });
    })();

    /**
     * Check if a Movement is a Fee.
     * Note that Fees are re-created from scratch AFTER you Save. So, here, they should be readonly.
     * In case, show also a nice explaination.
     * Otherwise, remove readonly, remove explaination.
     * This is useful in the page /movement/review.
     * @page /movement/review.
     * @param {Object} $context Whatever HTMLelement inside the Movement to be used as context.
     */
    function updateMovementForFee(context) {
        var $context = $(context);
        var $parent = $context.closest('.list-group-item');
        var $accountSelector = $parent.find('.ils-container-account-select select');
        var $userSelector = $parent.find('select.ils-user-select');

        // AccountRow ID - a Movement can have multiple AccountRow(s).
        // This is usually a number or the string "new".
        var accountRowIdRaw = $parent.find("input[name='account_row[]']").val();
        if (accountRowIdRaw === 'new' ) {
            accountRowIdRaw = "0";
        }
        var accountRowId = parseInt(accountRowIdRaw);
        var isExistingAccountRow = accountRowId > 0;

        // Account selector (bank Account, like "Fees")
        // This is usually a number greather than 0, or 0.
        var accountId = parseInt($accountSelector.find('option:selected').val());

        // User selector
        // This is usually a number greather than 0, or 0.
        var userId = parseInt($userSelector.find('option:selected').val());
        var isUserSelected = userId > 0;

        var isAccountForFee = accountId === CONFIG_ACCOUNT_FOR_FEES_ID;
        var willGenerateFee = !isExistingAccountRow && isAccountForFee && isUserSelected;

        // Eventually inform the user that this AccountRow will be automagically generated. Or not.
        $parent.find('.alert-account-2').toggle(willGenerateFee);
        $parent.find('.ils-account-amount, .ils-account-notes').attr('readonly', willGenerateFee);
    }

    // Trigger Fee stuff at startup (if this element exists in the page).
    // @page /movement/review.
    $('.list-group-item .ils-container-account-select').each(function() {
        updateMovementForFee(this);
    });

    // Trigger Fee stuff on Account change.
    // @page /movement/review.
    $('body').on('change', '.ils-container-account-select select', function() {
        updateMovementForFee(this);
    });

    // Trigger Fee stuff on User change.
    // @page /movement/review.
    $('body').on('change', 'select.ils-user-select', function() {
        updateMovementForFee(this);
    });

    $('body').on('change', '.max-selections input:checkbox', function(e) {
        e.preventDefault();
        var container = $(this).closest('.max-selections');
        var max = parseInt(container.attr('data-max-selections'));
        var choosen = container.find('input:checkbox').filter(':checked').length;

        if (choosen >= max) {
            container.find('input:checkbox').filter(':not(:checked)').prop('disabled', true);
        }
        else {
            container.find('input:checkbox').prop('disabled', false);
        }
    });

    // Auto-submit form on select change...
    $('.auto-filter-form select').change(function() {
        $(this).closest('form').submit();
    });

    // ...ora checkbox click
    $('.auto-filter-form input[type=checkbox]').click(function() {
        $(this).closest('form').submit();
    });

    if ($.fn.DataTable) {
        jQuery('.table').DataTable({
            paging: false,
            language: {
                url: '/js/datatables-it-IT.json',
            }
        });
    }

    $(document).keypress(function(e) {
        if ($(".modal").hasClass('show') && (e.keycode == 13 || e.which == 13)) {
            if (!$(".modal textarea").is(":focus")) {
                $(".modal .btn-primary").trigger('click');
            }
        }
    });

    $('#user-charts a, #user-list a').on('click', function (e) {
        e.preventDefault();
        $(this).tab('show');
    })
});
