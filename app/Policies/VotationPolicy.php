<?php

namespace App\Policies;

use App\User;
use App\Votation;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotationPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Votation $model)
    {
        return true;
    }

    public function create(User $user)
    {
        return $user->hasRole('admin');
    }

    public function update(User $user, Votation $model)
    {
        return $user->hasRole('admin');
    }

    public function delete(User $user, Votation $model)
    {
        return $user->hasRole('admin');
    }
}
