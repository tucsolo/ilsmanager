<?php
namespace App;

use Exception;

/**
 * A RefundStatus is a logical status of a Refund.
 */
class RefundStatus {

    /**
     * Internal cache of all well-known RefundStatus(es).
     * @var array
     */
    private static $all;

    public $id;
    public $uid;
    public $label;
    public $completed;
    public $percent;
    public $color;

    /**
     * Find a specific RefundStatus by its string UID.
     * @param string $uid like 'pending'
     * @return RefundStatus
     */
    public static function findByUID( $uid )
    {
        foreach( self::all() as $status ) {
            if ($status->uid === $uid) {
                return $status;
            }
        }
        throw new Exception(sprintf(
            "codice di stato di Rimborso sconosciuto: '%s'",
            $uid
        ));
    }

    /**
     * Find a specific RefundStatus by its ID.
     * @param int $id
     * @return RefundStatus
     */
    public static function findByID( $id )
    {
        foreach( self::all() as $status ) {
            if ($status->id === $id) {
                return $status;
            }
        }
        throw new Exception(sprintf(
            "ID di stato di rimborso sconosciuto: %d",
            $id
        ));
    }

    /**
     * Get all the known RefundStatus, indexed by ID.
     * This method has an internal cache. It's suitable to be called multiple times.
     * @return array
     */
    public static function all()
    {

        // Have an internal cache to be fast on consequent calls.
        if( !self::$all ) {
            self::$all = [];
            foreach( self::allRaw() as $id => $status_data ) {
                self::$all[] = self::array2object( $status_data, $id );
            }
        }

        return self::$all;
    }

    /**
     * Get all well-known RefundStatus in raw version.
     * @return array
     */
    protected static function allRaw()
    {

        // TODO: read these from a nice configuration :D Like a JSON somewhere or boh asd.
        $info[] = [
            'id'        => 0,
            'uid'       => 'waiting',
            'label'     => 'In attesa',
            'completed' => 0,
            'percent'   => 25,
            'color'     => 'warning',
        ];

        // Board approved.
        $info[] = [
            'id'        => 1,
            'uid'       => 'approved',
            'label'     => 'Approvato',
            'completed' => 0,
            'percent'   => 80,
            'color'     => 'warning',
        ];

        // Consensus missing from the board, etc.
        $info[] = [
            'id'        => 2,
            'uid'       => 'blocked',
            'label'     => 'Bloccato',
            'completed' => 0,
            'percent'   => 100,
            'color'     => 'danger',
        ];

        // Paid.
        $info[] = [
            'id'        => 3,
            'uid'       => 'paid',
            'label'     => 'Pagato',
            'completed' => 1,
            'percent'   => 100,
            'color'     => 'success',
        ];

        // Paid and also received in the payment logs.
        $info[] = [
            'id'        => 4,
            'uid'       => 'paid-verified',
            'label'     => 'Pagato e Verificato',
            'completed' => 1,
            'percent'   => 100,
            'color'     => 'success',
        ];

        return $info;
    }

    /**
     * Convert an associative array into a RefundStatus object.
     * @param array $data
     * @param int   $id
     * @return RefundStatus
     */
    private static function array2object($data, $id)
    {
        $status = new RefundStatus();
        $attributes = [ 'id', 'uid', 'label', 'completed', 'percent', 'color' ];
        foreach( $attributes as $attribute ) {
            $status->{ $attribute } = $data[ $attribute ];
        }
        return $status;
    }

}
