<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * The Fee is a single membership representation consisting in the tuple <User, Year>.
 * A Fee is created when a particular AccountRow is created.
 */
class Fee extends Model
{
    public function account_row()
    {
        return $this->belongsTo('App\AccountRow');
    }

    public function receipt()
    {
        $row = $this->account_row;
        if ($row)
            return $row->movement->receipt;
        else
            return null;
    }
}
