<?php

namespace App\Observers;

use Log;

use App\AccountRow;
use App\Config;
use App\Fee;

class AccountRowObserver
{
    private function assignFee($accountRow)
    {
        $user = $accountRow->user;

        if ($user) {
            $fee = new Fee();
            $fee->user_id = $accountRow->user_id;
            $fee->account_row_id = $accountRow->id;
            $fee->year = $accountRow->getFeeYear();
            $fee->save();

            if ($user->section != null) {
                $user->section->alterBalance(Config::getSectionQuote($accountRow->amount_in), $accountRow, $accountRow->notes);
            }

            if ($user->status != 'active' && $user->status != 'pending') {
                $user->status = 'active';
                $user->save();
            }

            if (Fee::where('user_id', $accountRow->user_id)->count() == 1) {
                $user->sendMail('App\Mail\FirstFee');
            }
        }
        else {
            Log::error('Movimento pagamento quota senza utente: ' . $accountRow->id);
        }
    }

    public function created(AccountRow $accountRow)
    {
        if ($accountRow->account_id == Config::feesAccount()->id) {
            $this->assignFee($accountRow);
        }
        else if ($accountRow->section_id != 0) {
            $accountRow->section->alterBalance($accountRow->amount, $accountRow, $accountRow->notes);
        }
    }

    public function updating(AccountRow $accountRow)
    {
        $original = AccountRow::find($accountRow->id);

        if ($accountRow->account_id == Config::feesAccount()->id && $original->user_id != $accountRow->user_id) {
            $original->user->fees()->orderBy('year', 'desc')->first()->delete();
            $this->assignFee($accountRow);
        }

        if ($original->section_id != $accountRow->section_id) {
            if ($accountRow->section_id != 0) {
                $accountRow->section->alterBalance($accountRow->amount, $accountRow, $accountRow->notes);
            }
            else {
                $original->section->alterBalance($accountRow->amount * -1, null, '[Annullato] ' . $accountRow->notes);
            }
        }
    }

    public function deleted(AccountRow $accountRow)
    {
        if ($accountRow->section_id != 0) {
            $accountRow->section->alterBalance($accountRow->amount * -1, null, '[Eliminato] ' . $accountRow->notes);
        }

        $fee = Fee::where('account_row_id', $accountRow->id)->first();
        if ($fee) {
            $fee->delete();

            if ($user->section) {
                $user->section->alterBalance(Config::getSectionQuote($accountRow->amount_in) * -1, $accountRow, '[Eliminato] ' . $accountRow->notes);
            }
        }
    }
}
