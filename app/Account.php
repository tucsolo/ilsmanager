<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * An Account is an internal Bank Account like "Donations" or "Fees" etc.
 * An Account may have a parent Account, for example "Goods" > "Web Services".
 */
class Account extends Model
{
    public function parent()
    {
        return $this->belongsTo('App\Account');
    }

    public function children()
    {
        return $this->hasMany('App\Account', 'parent_id');
    }

    public function getPrintableNameAttribute()
    {
        $names[] = $this->name;

        $iter = $this;
        while($parent = $iter->parent) {
            $names[] = $parent->name;
            $iter = $parent;
        }

        return join('/', array_reverse($names));
    }
}
