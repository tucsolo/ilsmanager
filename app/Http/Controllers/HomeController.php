<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class HomeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        $max_members = User::selectRaw('COUNT(id) AS count')->where('status', 'active')->groupBy('address_prov')->get()->max('count');

        return view('home', compact('max_members'));
    }
}
