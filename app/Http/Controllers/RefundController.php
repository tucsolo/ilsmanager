<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Refund;

class RefundController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Refund',
            'view_folder' => 'refund'
        ]);
    }

    protected function query($query, $id = null)
    {
        $user = Auth::user();

        if ($user->hasRole('admin') == false) {
            $query->where('user_id', $user->id);
        }

        $query->orderBy('updated_at', 'asc');

        return $query;
    }

    protected function requestToObject($request, $object)
    {
        $object->date = $request->input('date');
        $object->amount = $request->input('amount');
        $object->section_id = $request->input('section_id', 0);
        $object->notes = $request->input('notes');

        if ($request->user()->hasRole('admin')) {

            // The Status may not be expressed in creation mode.
            if($request->has('refunded')) {
                $object->refunded = $request->input('refunded');
            }

            $object->user_id = $request->input('user_id', 0);
            $object->admin_notes = $request->input('admin_notes');
        }
        else {
            $object->user_id = $request->user()->id;
            $object->admin_notes = $object->admin_notes ?: '';
        }

        return $object;
    }

    protected function defaultValidations($object)
    {
        return [
            'amount' => 'required|numeric',
            'notes' => 'required',
        ];
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    protected function afterSaving($request, $object)
    {
        if ($request->hasFile('file')) {
            $files = $request->file('file');

            foreach($files as $f) {
                $object->attachFile($f);
            }
        }
    }

    public function download(Request $request)
    {
        return response()->download(storage_path('refunds/' . $request->input('file')));
    }

    public function index()
    {
        //sovrascrivo il metodo index di EditController per passare altri dati necessari alla vista

        $request = request();
        
        $currentuser = Auth::user();

        $query = ($this->classname)::orderBy($this->defaultSortingColumn(), 'asc');
        $query = $this->query($query);
        $objects = $query->get();

        //region "#37 moved from blade file"

        // Note: the backend is "refunded" but at frontend is exposed as "status".
        $selected_statuses = [];
        $selected_refund_status_uids = $request->input('status');
        if(!is_array($selected_refund_status_uids)) {
            if($currentuser->hasRole('admin')) {
                // Default filters for reviewers :)
                $selected_refund_status_uids[] = 'waiting';   // Check incoming stuff to be approved.
                $selected_refund_status_uids[] = 'approved';  // Check approved stuff to be paid.
                $selected_refund_status_uids[] = 'paid';      // Check paid stuff to be also verified.
            } else {
                // Default filters for normal users :)
                $selected_refund_status_uids[] = 'waiting';   // See my pending stuff.
                $selected_refund_status_uids[] = 'approved';  // See my approved stuff.
                $selected_refund_status_uids[] = 'blocked';   // See my blocked stuff so I can change things to make the board happier.
            }
        }
        foreach($selected_refund_status_uids as $selected_refund_status_uid) {
            $selected_statuses[] = \App\RefundStatus::findByUID($selected_refund_status_uid);
        }
        if($request->input('status')) {
            // WHERE status IN ( ... )
            $selected_status_ids = [];
            foreach($selected_statuses as $status) {
                $selected_status_ids[$status->id] = $status->id;
            }
            $objects = $objects->whereIn('refunded', $selected_status_ids);
        }

        $min_date = \App\Refund::min('date');
        $max_date = \App\Refund::max('date');

        $min_year = date('Y', strtotime($min_date));
        $max_year = date('Y', strtotime($max_date));

        $users = [];
        if($currentuser->hasRole('admin')) {
            $users = \App\User::whereIn('status', ['active'])->has('refunds')->orderBy('username', 'asc')->get();
            if($request->input('user_id')) {
                $objects = $objects->where('user_id', $request->input('user_id'));
            }
        }

        $sections = \App\Section::orderBy('city', 'asc')->get();
        if($request->input('section_id')) {
            $objects = $objects->where('section_id', $request->input('section_id'));
        }

        if($request->input('year')) {
            $objects = $objects->filter(function ($object) use ($request){
                return date('Y', strtotime($object->date)) == $request->input('year');
            });
        }

        $all_statuses = \App\RefundStatus::all();

        //region calculate the total amount of refunds
        $total = 0;
        $total_refunded = 0;
        foreach($objects as $refund) {
            $total += $refund->amount;
            $total_refunded += ($refund->amount - ($refund->getRefundStatus()->completed ? $refund->amount : 0));
        }
        //endregion calculate the total amount of refunds

        //endregion "#37 moved from blade file"

        return view($this->view_folder . '.index', [
            'objects' => $objects,
            'classname' => $this->classname,
            'view_folder' => $this->view_folder,
            'sorting' => $this->defaultSortingColumn(),
            'direction' => 'asc',
            'search' => '',
            'min_year' => $min_year,
            'max_year' => $max_year,
            'selected_statuses' => $selected_statuses,
            'users' => $users,
            'sections' => $sections,
            'all_statuses' => $all_statuses,
            'selected_refund_status_uids' => $selected_refund_status_uids,
            'total' => $total,
            'total_refunded' => $total_refunded
        ]);
    }
}
