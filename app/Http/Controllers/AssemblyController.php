<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Assembly;

class AssemblyController extends EditController
{
    public function __construct()
    {
        parent::init([
            'classname' => 'App\Assembly',
            'view_folder' => 'assembly'
        ]);
    }

    protected function requestToObject($request, $object)
    {
        if ($request->user()->hasRole('admin')) {
            $fields = ['date', 'status', 'announce'];
            $object = $this->fitObject($object, $fields, $request);
        }

        return $object;
    }

    protected function defaultValidations($object)
    {
        return [];
    }

    protected function defaultSortingColumn()
    {
        return 'date';
    }

    public function edit($id)
    {
        return redirect()->route('assembly.index');
    }

    public function partecipate(Request $request, $assembly_id, $do)
    {
        $user = $request->user();
        $assembly = Assembly::find($assembly_id);
        $assembly->users()->detach($user->id);

        foreach($assembly->users()->wherePivot('delegate_id', $user->id)->get() as $delegates) {
            $assembly->users()->detach($delegates->id);
        }

        if ($do != 0) {
            $assembly->users()->attach($user->id);
        }

        return redirect()->route('assembly.index');
    }

    public function delegate(Request $request, $assembly_id, $delegate_id)
    {
        $user = $request->user();
        $assembly = Assembly::find($assembly_id);
        $assembly->users()->detach($user->id);

        if ($delegate_id != 0) {
            $assembly->users()->attach($user->id, ['delegate_id' => $delegate_id]);
        }

        return redirect()->route('assembly.index');
    }
}
