<input type="hidden" name="assembly_id" value="{{ $object ? $object->assembly_id : $assembly_id }}">

<div class="form-group row">
    <label for="question" class="col-sm-4 col-form-label">Quesito</label>
    <div class="col-sm-8">
        <textarea class="form-control" name="question">{{ $object ? $object->question : '' }}</textarea>
    </div>
</div>

<div class="form-group row">
    <label for="status" class="col-sm-4 col-form-label">Stato</label>
    <div class="col-sm-8">
        <select class="form-control" name="status">
            <option value="pending" {{ $object && $object->status == 'pending' ? 'selected' : '' }}>In Attesa</option>
            <option value="running" {{ $object && $object->status == 'running' ? 'selected' : '' }}>Aperta</option>
            <option value="closed" {{ $object && $object->status == 'closed' ? 'selected' : '' }}>Chiusa</option>
        </select>
    </div>
</div>

<div class="form-group row">
    <label for="max_options" class="col-sm-4 col-form-label">Numero Opzioni Selezionabili</label>
    <div class="col-sm-8">
        <input type="number" step="1" min="1" max="100" class="form-control" name="max_options" value="{{ $object ? $object->max_options : 1 }}">
    </div>
</div>

<div class="form-group row">
    <label for="options" class="col-sm-4 col-form-label">Opzioni</label>
    <div class="col-sm-8 multiple-table">
        <ul class="list-group">
            @if($object)
                @foreach($object->options as $option)
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="option[]" value="{{ $option }}">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-danger delete-row">Rimuovi</button>
                            </div>
                        </div>
                    </li>
                @endforeach
            @endif

            <li class="list-group-item">
                <button class="btn btn-success add-row">Aggiungi</button>
                <ul class="hidden">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="option[]" value="">
                            </div>
                            <div class="col-md-2">
                                <button class="btn btn-danger delete-row">Rimuovi</button>
                            </div>
                        </div>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
