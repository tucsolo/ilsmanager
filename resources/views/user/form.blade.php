<div class="form-group row">
    <label for="name" class="col-sm-4 col-form-label">Nome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="name" value="{{ $object ? $object->name : '' }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="surname" class="col-sm-4 col-form-label">Cognome</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="surname" value="{{ $object ? $object->surname : '' }}" required>
    </div>
</div>

<div class="form-group row">
    <label for="username" class="col-sm-4 col-form-label">Nickname</label>
    <div class="col-sm-8">
        @if($currentuser->hasRole('admin'))
            <input type="text" class="form-control" name="username" value="{{ $object ? $object->username : '' }}">
        @else
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->username : '' }}">
        @endif
    </div>
</div>

<div class="form-group row">
    <label for="password" class="col-sm-4 col-form-label">Password</label>
    <div class="col-sm-8">
        <input type="password" class="form-control" name="password" placeholder="Lascia in bianco per non modificare" autocomplete="off">
    </div>
</div>
<div class="form-group row">
    <label for="email" class="col-sm-4 col-form-label">E-Mail</label>
    <div class="col-sm-8">
        <input type="email" class="form-control" name="email" value="{{ $object ? $object->email : '' }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="phone" class="col-sm-4 col-form-label">Telefono</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="phone" value="{{ $object ? $object->phone : '' }}">
    </div>
</div>
<div class="form-group row">
    <label for="website" class="col-sm-4 col-form-label">Sito Web</label>
    <div class="col-sm-8">
        <input type="url" class="form-control" name="website" value="{{ $object ? $object->website : '' }}">
        <small class="form-text text-muted">Se il sito è dotato di feed RSS, sarà automaticamente incluso in <a href="https://planet.linux.it/ils/">Planet ILS</a>. L'indice viene aggiornato una volta alla settimana.</small>
    </div>
</div>

<hr>

@if(App\Config::getConfig('custom_email_aliases') == '1')
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">E-Mail Personale</legend>
            <div class="col-sm-8">
                <div class="form-check">
                    <label class="form-check-label" for="alias">
                        <input class="form-check-input" type="radio" name="email_behaviour" id="alias" value="alias" {{ $object && $object->getConfig('email_behaviour') ? 'checked' : '' }}>
                            Alias di posta: le email saranno forwardate all'indirizzo sopra configurato
                    </label>
                </div>
                <div class="form-check">
                    <label class="form-check-label" for="email_behaviour">
                        <input class="form-check-input" type="radio" id="email_behaviour" name="email_behaviour" value="inbox" {{ $object && $object->getConfig('email_behaviour') == 'inbox' ? 'checked' : '' }}>
                        {!! nl2br(App\Config::getConfig('custom_email_aliases_message')) !!}
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="email_password" class="col-sm-4 col-form-label">Password E-Mail Personale</label>
        <div class="col-sm-8">
            <input type="password" class="form-control" name="email_password" placeholder="Lascia in bianco per non modificare">
        </div>
    </div>

    <div class="form-group row">
        <div class="offset-4 col-sm-8">
            <small class="form-text text-muted">Se scegli di avere una inbox locale, devi definire una password. Questa <strong>non</strong> corrisponde alla password di ILSManager.</small>
            <small class="form-text text-muted">Queste configuzioni vengono applicate sul mail server ogni 3 ore: se le modifichi, aspetta un poco prima di provarle!</small>
        </div>
    </div>

    <hr>
@endif

<div class="form-group row">
    <div class="col-sm-4">Volontario</div>
    <div class="col-sm-8">
        <div class="form-check">
            <input class="form-check-input" type="checkbox" name="volunteer" {{ $object && $object->volunteer ? 'checked' : '' }}>
            <label class="form-check-label" for="volunteer">
                Iscritto al Registro dei Volontari
            </label>
        </div>
    </div>
</div>

@if(App\Section::count() != 0)
    <div class="form-group row">
        <label for="section_id" class="col-sm-4 col-form-label">Sezione Locale</label>
        <div class="col-sm-8">
            @include('section.select', ['select' => $object ? $object->section_id : 0, 'name' => 'section_id'])
        </div>
    </div>

    <hr>
@endif

<div class="form-group row">
    <label for="taxcode" class="col-sm-4 col-form-label">Codice Fiscale</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="taxcode" value="{{ $object ? $object->taxcode : '' }}" required>
    </div>
</div>
<div class="form-group row">
    <label for="birth_place" class="col-sm-4 col-form-label">Luogo di Nascita</label>
    <div class="col-sm-5">
        <input type="text" class="form-control" name="birth_place" value="{{ $object ? $object->birth_place : '' }}" required>
    </div>
    <div class="col-sm-3">
        @include('commons.provinces', ['name' => 'birth_prov', 'value' => $object ? $object->birth_prov : null])
    </div>
</div>
<div class="form-group row">
    <label for="birth_date" class="col-sm-4 col-form-label">Data di Nascita</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="birth_date" value="{{ $object ? $object->birth_date : '' }}" placeholder="YYYY-MM-DD" required>
    </div>
</div>

<hr>

<div class="form-group row">
    <label for="address_street" class="col-sm-4 col-form-label">Indirizzo di Residenza</label>
    <div class="col-sm-8">
        <input type="text" class="form-control" name="address_street" value="{{ $object ? $object->address_street : '' }}" placeholder="e.g. Via Roma 42" required>
    </div>
</div>
<div class="form-group row">
    <label for="address_place" class="col-sm-4 col-form-label">Città di Residenza</label>
    <div class="col-sm-3">
        <input type="text" class="form-control" name="address_place" value="{{ $object ? $object->address_place : '' }}" placeholder="Città" required>
    </div>
    <div class="col-sm-2">
        <input type="text" class="form-control" name="address_zip" value="{{ $object ? $object->address_zip : '' }}" placeholder="CAP" required>
    </div>
    <div class="col-sm-3">
        @include('commons.provinces', ['name' => 'address_prov', 'value' => $object ? $object->address_prov : null])
    </div>
</div>

<hr>

<div class="card">
    <div class="card-body">
        <div class="form-group row">
            <label for="shipping_name" class="col-sm-4 col-form-label">Spedizione Presso</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="shipping_name" value="{{ $object ? $object->shipping_name : '' }}" placeholder="e.g. c/o Azienda XYZ">
            </div>
        </div>
        <div class="form-group row">
            <label for="shipping_street" class="col-sm-4 col-form-label">Indirizzo di Spedizione</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" name="shipping_street" value="{{ $object ? $object->shipping_street : '' }}" placeholder="e.g. Via Roma 42">
            </div>
        </div>
        <div class="form-group row">
            <label for="shipping_place" class="col-sm-4 col-form-label">Città di Spedizione</label>
            <div class="col-sm-3">
                <input type="text" class="form-control" name="shipping_place" value="{{ $object ? $object->shipping_place : '' }}" placeholder="Città">
            </div>
            <div class="col-sm-2">
                <input type="text" class="form-control" name="shipping_zip" value="{{ $object ? $object->shipping_zip : '' }}" placeholder="CAP">
            </div>
            <div class="col-sm-3">
                @include('commons.provinces', ['name' => 'shipping_prov', 'value' => $object ? $object->shipping_prov : null])
            </div>
        </div>
        <div class="form-group row">
            <div class="offset-4 col-sm-8">
                <small class="form-text text-muted">Se l'indirizzo di spedizione non viene esplicitato, viene usato quello di residenza.</small>
            </div>
        </div>

        <hr>

        <div class="form-group row">
            <label for="size" class="col-sm-4 col-form-label">Taglia</label>
            <div class="col-sm-8">
                <select name="size" class="form-control">
                    <option value="none">Non Selezionato</option>
                    @foreach(['S', 'M', 'L', 'XL', 'XXL'] as $size)
                        <option value="{{ $size }}" {{ $object && $object->size == $size ? 'selected' : '' }}>{{ $size }}</option>
                    @endforeach
                </select>
            </div>
        </div>
    </div>
</div>

<hr>

@if($currentuser->hasRole('admin'))
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Tipo</legend>
            <div class="col-sm-8">
                @foreach(App\User::types() as $type)
                    <div class="form-check">
                        <label class="form-check-label" for="{{ $type->identifier }}">
                            <input class="form-check-input" type="radio" name="type" id="{{ $type->identifier }}" value="{{ $type->identifier }}" {{ $object && $object->type == $type->identifier ? 'checked' : '' }}>
                                {{ $type->label }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Stato</legend>
            <div class="col-sm-8">
                @foreach(App\User::statuses() as $status)
                <div class="form-check">
                        <label class="form-check-label" for="{{ $status->identifier }}">
                            <input class="form-check-input" type="radio" name="status" id="{{ $status->identifier }}" value="{{ $status->identifier }}" {{ $object && $object->status == $status->identifier ? 'checked' : '' }}>
                                {{ $status->label }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-sm-4 pt-0">Ruolo</legend>
            <div class="col-sm-8">
                @foreach(App\Role::orderBy('name', 'asc')->get() as $role)
                <div class="form-check">
                <label class="form-check-label" for="{{ $role->id }}">
                            <input class="form-check-input" type="checkbox" name="roles[]" id="{{ $role->id }}" value="{{ $role->id }}" {{ $object && $object->roles()->where('roles.id', $role->id)->first() ? 'checked' : '' }}>
                                {{ $role->name }}
                        </label>
                    </div>
                @endforeach
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="notes" class="col-sm-4 col-form-label">Note</label>
        <div class="col-sm-8">
            <textarea class="form-control" name="notes">{{ $object ? $object->notes : '' }}</textarea>
        </div>
    </div>

    <hr>

    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Registrazione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="request_at" value="{{ $object ? $object->request_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>
    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Approvazione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="approved_at" value="{{ $object ? $object->approved_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>
    <div class="form-group row">
        <label for="birth_date" class="col-sm-4 col-form-label">Data di Espulsione</label>
        <div class="col-sm-8">
            <input type="text" class="form-control" name="expelled_at" value="{{ $object ? $object->expelled_at : '' }}" placeholder="YYYY-MM-DD">
        </div>
    </div>

    @if($object)
        <hr>
        <a href="{{ route('user.bypass', $object->id) }}" class="btn btn-danger">Impersona</a>
    @endif
@else
    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Tipo</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->human_type : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Stato</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->human_status : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Data di Registrazione</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->request_at : '' }}">
        </div>
    </div>

    <div class="form-group row">
        <label class="col-sm-4 col-form-label">Data di Approvazione</label>
        <div class="col-sm-8">
            <input type="text" readonly class="form-control-plaintext" value="{{ $object ? $object->approved_at : '' }}">
        </div>
    </div>
@endcan
