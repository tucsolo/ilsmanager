<p>
    È stato registrato il versamento della tua quota: ti diamo il benvenuto in {{ App\Config::getConfig('association_name') }}!
</p>
<p>
    Da adesso puoi consultare lo stato della tua registrazione e modificare il tuo profilo direttamente dal gestionale dei soci. Dalla pagina<br>
    <a href="{{ route('password.request') }}">{{ route('password.request') }}</a><br>
    puoi impostare una nuova password di accesso, mentre da<br>
    <a href="{{ route('login') }}">{{ route('login') }}</a><br>
    puoi effettuare l'accesso. Il tuo nickname è "{{ $user->username }}".
</p>

@if(App\Config::getConfig('custom_email_aliases') == '1') {
    <p>
        Nelle prossime ore sarà anche attivato il tuo nuovo indirizzo di posta {{ $user->custom_email }} che potrai gestire autonomamente sempre dal gestionale dei soci. In assenza di preferenze, diventerà un semplice reindirizzamento alla casella di posta che già avevi. Comodo, no?
    </p>
@endif

<p>
    Grazie per la tua adesione a {{ App\Config::getConfig('association_name') }}!
</p>
