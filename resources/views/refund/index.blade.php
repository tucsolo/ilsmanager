@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createUser">Nuovo Rimborso Spese</button>
            <div class="modal fade" id="createUser" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Nuovo Rimborso Spese</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('refund.store') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="modal-body">
                                @include('refund.form', ['object' => null])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form method="GET" action="" class="auto-filter-form">
        <div class="row mt-3">
            <div class="col-md-3">
                <div class="form-group">
                    <label for="refunded">Stato:</label>

                    @foreach($all_statuses as $status)
                        <div>
                            <input id="refund-uid-{{ $status->uid }}" type="checkbox" name="status[]" value="{{ $status->uid }}" @checked(in_array($status->uid, $selected_refund_status_uids))>
                            <label for="refund-uid-{{ $status->uid }}">{{ $status->label }}</label>
                        </div>
                    @endforeach
                </div>
            </div>

            @if($currentuser->hasRole('admin'))
            <div class="col-md-3">
                <div class="form-group">
                    <label for="user_id">Socio:</label>
                    <select id="user_id" name="user_id" class="form-control">
                        <option value="">Tutti</option>

                        @foreach($users as $user)
                            <option value="{{ $user->id }}" @selected(app('request')->input('user_id') == $user->id)>{{ $user->printable_name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            @endif

            <div class="col-md-3">
                <div class="form-group">
                    <label for="section_id">Sezione:</label>
                    <select id="section_id" name="section_id" class="form-control">
                        <option value="">Tutte</option>

                        @foreach($sections as $section)
                            <option value="{{ $section->id }}" @selected(app('request')->input('section_id') == $section->id)>{{ $section->city }}</option>
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label for="year">Anno:</label>

                    <select name="year" class="form-control">
                        <option value="">Tutti</option>

                        @for($y=$max_year; $y>=$min_year; $y--)
                            <option value="{{ $y }}" @selected(app('request')->input('year') == $y)>{{ $y }}</option>
                        @endfor
                    </select>
                </div>
            </div>
        </div>
    </form>

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Data</th>
                        <th>Socio</th>
                        <th>Sezione</th>
                        <th>Importo</th>
                        <th>Da rimborsare</th>
                        <th>Stato</th>
                        <th>Dettagli</th>
                    </tr>
                </thead>
                <tbody>
                    
                    @foreach($objects as $refund)
                        <tr>
                            <td>{{ $refund->id }}</td>
                            <td>{{ $refund->date }}</td>
                            <td>{{ $refund->user ? $refund->user->printable_name : '' }}</td>
                            <td>{{ $refund->section ? $refund->section->city : '' }}</td>
                            <td class="text-right">{{ $refund->amount }}</td>
                            <td class="text-right">{{ $refund->getRefundStatus()->completed ? '0.00' : $refund->amount }}</td>
                            <td class="text-center">
                                {{ $refund->getRefundStatus()->label }}
                                <br>
                                <div class="progress mini-progress-bar">
                                    <div class="progress-bar bg-{{ $refund->getRefundStatus()->color }}" role="progressbar" style="width: {{ $refund->getRefundStatus()->percent }}%;" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </td>
                            <td>
                                <a href="{{ route('refund.edit', $refund->id) }}"><span class="oi oi-pencil"></span></a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>

                <tfoot>
                    <tr>
                        <td colspan="4" class="text-right">Totale</td>
                        <td class="text-right">{{ number_format($total, 2) }}</td>
                        <td class="text-right">{{ number_format($total_refunded, 2) }}</td>
                        <td colspan="2"></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
