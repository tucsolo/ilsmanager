@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <h3 class="text-center">
                Unisciti alla più grande associazione italiana di promozione e divulgazione di Linux e del software libero,
            </h3>
            <h2 class="text-center">
                aderisci a Italian Linux Society!
            </h2>

            <hr>

            <p>
                Tutti i soci hanno diritto a
            </p>
            <ul>
                <li>un indirizzo di posta <i>nome.cognome@linux.it</i> ed uno <i>nome.cognome@ils.org</i> (o simile, in caso di omonimie)</li>
                <li>un indirizzo di posta <i>nickname@linux.it</i> ed uno <i>nickname@ils.org</i> (previa disponibilità del nome scelto)</li>
                <!--
                <li>spazio web per l'hosting di applicazioni e la pubblicazione di contenuti</li>
                <li>un dominio <i>nomeascelta.linux.it</i></li>
                -->
                <li>una mailing list <i>nomeascelta@lists.linux.it</i></li>
                <li>accesso alla mailing list privata dei Soci</li>
                <li>aggregazione del proprio sito o blog su <a href="https://planet.linux.it/">Planet ILS</a></li>
            </ul>

            <p>
                <u>La quota di iscrizione annuale è di <strong>{{ App\Config::getConfig('regular_annual_fee') }} euro</strong></u> e può essere versata tramite:
            </p>
            <ul>
                <li>accredito sul conto PayPal intestato a direttore@linux.it</li>
                <li>bonifico bancario sull'IBAN IT74G0200812609000100129899 intestato a "ILS Italian Linux Society"</li>
            </ul>

            <p>
                Per l'iscrizione come Socio Ordinario compila il form di richiesta di iscrizione qui sotto e avrai tutte le informazioni necessarie per proseguire. <br>
                La procedura per segnare i pagamenti della quota è manuale e non periodica, quindi abbiate pazienza. Dopo il versamento della quota riceverai una mail di conferma: se non ti arriva entro quindici giorni, o se hai dubbi o domande sulla procedura di iscrizione, scrivi a <strong>{{ App\Config::getConfig('association_email') }}</strong>.<br>
                Scrivi allo stesso indirizzo anche <a href="https://www.ils.org/info#aderenti">se vuoi iscrivere la tua Associazione</a> o <a href="https://www.ils.org/sponsorizzazioni">se la tua azienda vuole diventare sponsor di Italian Linux Society</a>.
            </p>

            @if(date('m') >= 10)
                <div class="alert alert-info text-center">
                    Iscrivendoti adesso, la tua quota vale già per l'anno {{ date('Y') + 1 }}!
                </div>
            @endif

            <br>

            <div class="card">
                <div class="card-header">Modulo di Iscrizione a Italian Linux Society</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">Nome</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="surname" class="col-md-4 col-form-label text-md-right">Cognome</label>

                            <div class="col-md-6">
                                <input id="surname" type="text" class="form-control{{ $errors->has('surname') ? ' is-invalid' : '' }}" name="surname" value="{{ old('surname') }}" required>

                                @if ($errors->has('surname'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('surname') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">Indirizzo E-Mail</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="username" class="col-md-4 col-form-label text-md-right">Nickname</label>

                            <div class="col-md-6">
                                <input id="username" type="text" class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" name="username" value="{{ old('username') }}" required>
                                <small class="form-text text-muted">Viene usato per assegnare l'indirizzo mail <i>nickname@linux.it</i> e per accedere al gestionale interno dei soci.</small>

                                @if ($errors->has('username'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_date" class="col-md-4 col-form-label text-md-right">Data di Nascita</label>

                            <div class="col-md-6">
                                <input id="birth_date" type="text" class="form-control date {{ $errors->has('birth_date') ? 'is-invalid' : '' }}" name="birth_date" value="{{ old('birth_date') }}" placeholder="YYYY-MM-DD" required>

                                @if ($errors->has('birth_date'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth_date') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_place" class="col-md-4 col-form-label text-md-right">Comune di Nascita</label>

                            <div class="col-md-4">
                                <input id="birth_place" type="text" class="form-control{{ $errors->has('birth_place') ? ' is-invalid' : '' }}" name="birth_place" value="{{ old('birth_place') }}" required>
                                <small class="form-text text-muted">Inserisci la tua provincia se non sei nato all'estero, non lasciare <b>Estero</b>.</small>

                                @if ($errors->has('birth_place'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('birth_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @include('commons.provinces', ['name' => 'birth_prov', 'value' => old('birth_prov')])
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="taxcode" class="col-md-4 col-form-label text-md-right">Codice Fiscale</label>

                            <div class="col-md-6">
                                <input id="taxcode" type="text" class="form-control{{ $errors->has('taxcode') ? ' is-invalid' : '' }}" name="taxcode" value="{{ old('taxcode') }}" required>

                                @if ($errors->has('taxcode'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('taxcode') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="address_street" class="col-md-4 col-form-label text-md-right">Indirizzo di Residenza</label>

                            <div class="col-md-6">
                                <input id="address_street" type="text" class="form-control{{ $errors->has('address_street') ? ' is-invalid' : '' }}" name="address_street" value="{{ old('address_street') }}" required>

                                @if ($errors->has('address_street'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address_street') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="birth_place" class="col-md-4 col-form-label text-md-right">Comune di Residenza</label>

                            <div class="col-md-2">
                                <input id="address_place" type="text" class="form-control{{ $errors->has('address_place') ? ' is-invalid' : '' }}" name="address_place" value="{{ old('address_place') }}" required>
                                <small class="form-text text-muted">Inserisci la tua provincia se non vivi all'estero, non lasciare <b>Estero</b>.</small>

                                @if ($errors->has('address_place'))
                                    <span class="invalid-feedback">
                                        <strong>{{ $errors->first('address_place') }}</strong>
                                    </span>
                                @endif
                            </div>
                            <div class="col-md-2">
                                @include('commons.provinces', ['name' => 'address_prov', 'value' => old('address_prov')])
                            </div>
                            <label for="address_zip" class="col-lg-12 col-form-label d-md-none">CAP di Residenza</label>
                            <div class="col-md-2">
                                <input id="address_zip" type="text" class="form-control{{ $errors->has('address_zip') ? ' is-invalid' : '' }}" name="address_zip" value="{{ old('address_zip') }}" placeholder="00000" required>
                            </div>
                        </div>

                        @if(App\Section::all()->isEmpty() == false)
                            <div class="form-group row">
                                <label for="address_street" class="col-md-4 col-form-label text-md-right">Sezione Locale</label>

                                <div class="col-md-6">
                                    <select id="address_street" class="form-control{{ $errors->has('section_id') ? ' is-invalid' : '' }}" name="section_id">
                                        <option value="">Nessuna</option>
                                        @foreach(App\Section::orderBy('city', 'asc')->get() as $section)
                                            <option value="{{ $section->id }}">{{ $section->city }} ({{ $section->prov }})</option>
                                        @endforeach
                                    </select>
                                    <small class="form-text text-muted">Selezionando la <a href="https://www.ils.org/sezionilocali">sezione locale</a> di riferimento parte della tua quota di iscrizione annuale andrà a sostenere le attività di promozione e divulgazione nella città scelta.</small>

                                    @if ($errors->has('section_id'))
                                        <span class="invalid-feedback">
                                            <strong>{{ $errors->first('section_id') }}</strong>
                                        </span>
                                    @endif
                                </div>
                            </div>
                        @endif

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="statuto" name="statuto" required>
                                    <label class="form-check-label" for="statuto">
                                        Ho letto lo <a href="http://www.ils.org/statuto" target="_blank">Statuto</a> e vi aderisco.
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-10 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="privacy" name="privacy" required>
                                    <label class="form-check-label" for="privacy">
                                        Ho letto l'<a href="http://www.ils.org/privacy" target="_blank">Informativa Privacy</a> e la accetto.
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Registrati a Italian Linux Society
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
