<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    if (Auth::check())
        return redirect()->route('home');
    else
        return redirect()->route('login');
});

Auth::routes();
Route::get('register/thanks', 'Auth\RegisterController@thanks')->name('register.thanks');

Route::middleware(['auth'])->group(function () {
    Route::get('home', 'HomeController@index')->name('home');

    Route::post('user/approve', 'UserController@approve')->name('user.approve');
    Route::get('user/bypass/{id}', 'UserController@bypass')->name('user.bypass');

    Route::get('movement/review', 'MovementController@review')->name('movement.review');
    Route::post('movement/attach/{id}', 'MovementController@attach')->name('movement.attach');
    Route::post('movement/refund/{id}', 'MovementController@refund')->name('movement.refund');
    Route::get('movement/download', 'MovementController@download')->name('movement.download');

    Route::get('section/{id}/balance', 'SectionController@balance')->name('section.balance');
    Route::post('section/{id}/exportmembers', 'SectionController@exportmembers')->name('section.exportmembers');

    Route::get('refund/download', 'RefundController@download')->name('refund.download');

    Route::get('sponsor/logo/{id}', 'SponsorController@logo')->name('sponsor.logo');

    Route::get('doc/download', 'DocumentationController@download')->name('doc.download');
    Route::get('doc/link', 'DocumentationController@link')->name('doc.link');
    Route::get('doc/preview', 'DocumentationController@preview')->name('doc.preview');

    Route::post('assembly/partecipate/{assembly_id}/{do}', 'AssemblyController@partecipate')->name('assembly.partecipate');
    Route::post('assembly/delegate/{assembly_id}/{delegate_id}', 'AssemblyController@delegate')->name('assembly.delegate');

    Route::get('bank/list/', 'BankController@list')->name('bank.list_all');
    Route::get('bank/list/{id}', 'BankController@list')->name('bank.list');

    Route::resource('user', 'UserController');
    Route::resource('volunteer', 'VolunteerController');
    Route::resource('doc', 'DocumentationController');
    Route::resource('config', 'ConfigController');
    Route::resource('bank', 'BankController');
    Route::resource('movement', 'MovementController');
    Route::resource('rule', 'RuleController');
    Route::resource('section', 'SectionController');
    Route::resource('receipt', 'ReceiptController');
    Route::resource('refund', 'RefundController');
    Route::resource('assembly', 'AssemblyController');
    Route::resource('votation', 'VotationController');
    Route::resource('vote', 'VoteController');
    Route::resource('sponsor', 'SponsorController');
});
