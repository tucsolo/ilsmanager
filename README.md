# ILSManager

Questo è il codice sorgente del gestionale interno dell'associazione Italian Linux Society.

Puoi vederlo online qui:

https://ilsmanager.linux.it/

## Avvio con Docker Compose

```
# Da fare solo la prima volta
cp .env.example .env

# Avvio
docker-compose up --build

# Avvio (se non funziona l'altro)
docker compose up
```

A questo punto visita questo indirizzo:

http://127.0.0.1:28000

Puoi cambiare la porta modificando il tuo file `.env`. Questa porta di default è stata scelta per evitarti collisioni se utilizzi altri repository Italian Linux Society. [Info](https://gitlab.com/ItalianLinuxSociety/ils-infrastructure/-/blob/main/DOCKER_GUIDELINES.md).

Se invece NON vuoi usare Docker, prosegui con la prossima sezione sull'installazione.

## Avvio Nativo

Esempio di installazione e avvio nativo su Debian:

```
# Installazione dipendenze di sistema (solo la prima volta)
sudo apt install php-pdo php-dom php-curl # prod
sudo apt install composer php-cli         # plus dev

# Installazione dipendenze PHP (solo la prima volta)
composer install

# Configurazione ambiente (solo la prima volta)
# Dovrai inserire le credenziali ad una MariaDB in tuo possesso!
cp .env.example .env

# Inserisci credenziali del tuo database MariaDB
# come ad esempio DB_HOST=localhost ecc. (dovresti sapere tu queste cose)
vim .env

# Popolamento database (solo la prima volta)
php artisan migrate
php artisan key:generate
php artisan db:seed

# Avvio
php artisan serve
```

Segui le istruzioni a schermo.

## Credenziali di test

Di default vengono create queste credenziali:

```
user        |    psw

admin       |    admin
member      |    member
referent    |    referent
pending     |    pending
suspended   |    suspended
expelled    |    expelled
dropped     |    dropped
association |    association
```

## Mailhog

https://github.com/mailhog/MailHog

```
MAIL_DRIVER=smtp
MAIL_HOST=0.0.0.0
MAIL_PORT=1025
MAIL_USERNAME=null
MAIL_PASSWORD=null
MAIL_ENCRYPTION=null
```

## PayPal

Puoi configurare la lettura automatica da un conto PayPal modificando il file `.env` con:

```
PAYPAL_USERNAME=
PAYPAL_PASSWORD=
PAYPAL_SIGNATURE=
PAYPAL_BANKID=
```

Oltre alle informazioni dalle API PayPal, BANKID è l'ID nel database del conto PayPal.

## API

ILS Manager espone alcune API per scaricare in blocco alcuni dati. Esempi:

    https://ilsmanager.linux.it/ng/api/websites?token=<token>
    https://ilsmanager.linux.it/ng/api/emails?token=<token>
    ...

Tutte queste API richiedono il token chiamato `API_TOKEN`. Utilizzi noti:

* `websites`
    * utilizzata da https://planet.linux.it/ per mostrare gli ultimi articoli delle persone associate a ILS
* `emails`
    * utilizzata dal server `kirk` per creare gli indirizzi di posta elettronica @linux.it e @ils.org
* ...

## Contributori

Persone che mantengono il codice da poter contattare per domande:

* Roberto Guido (autore e sviluppatore principale)
* Daniele Scasciafratte (cose + Laravel)
* Valerio Bozzolan (cose + Docker)

Grazie a tutte le persone che hanno contribuito:

https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/graphs/master

## Licenza

Copyright (C) 2019-2024 Italian Linux Society and contributors

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
See the GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License along with this program. If not, see <https://www.gnu.org/licenses/>.
